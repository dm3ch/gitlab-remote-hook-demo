# Hook wrapper server
This is very simple Golang app that handles http rpc requests.

For each RPC call it executes configured hook command, waits it completion and return results in http response.

# Environment variables
`COMMAND` - command that would be executed on each RPC call
`PORT` - (default: 9999) port on which http server would be started