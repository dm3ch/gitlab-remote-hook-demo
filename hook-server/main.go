package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	args []string
)

func healthz(w http.ResponseWriter, req *http.Request) {
	log.Info("Healthz request")
	fmt.Fprintf(w, "I'm alive!\n")
}

func executeCommand(extraArgs []string, env map[string]string) (string, string, int) {
	var cmdEnv []string
	var cmdCommand = args[0]
	var cmdArgs = args[1:]

	cmdArgs = append(cmdArgs, extraArgs...)
	for k, v := range env {
		cmdEnv = append(cmdEnv, fmt.Sprintf("%s=%s", k, v))
	}

	var stdout bytes.Buffer
	var stderr bytes.Buffer
	var exitCode = 0

	cmd := exec.Command(cmdCommand, cmdArgs...)
	cmd.Env = cmdEnv
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			exitCode = exitError.ExitCode()
		} else {
			log.WithError(err).Error("Unhandled error in executeCommand")
		}
	}

	return stdout.String(), stderr.String(), exitCode
}

type HookRequest struct {
	Args []string          `json:"args"`
	Env  map[string]string `json:"env"`
}

type HookResponse struct {
	ExitCode int    `json:"exitCode"`
	StdOut   string `json:"stdOut"`
	StdErr   string `json:"stdErr"`
}

func hook(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	var req HookRequest
	err := dec.Decode(&req)
	if err != nil {
		log.WithError(err).Error("Unhandled error in jsonDecode")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	startTime := time.Now()
	stdOut, stdErr, exitCode := executeCommand(req.Args, req.Env)
	endTime := time.Now()

	log.WithFields(log.Fields{
		"hookDuration": endTime.Sub(startTime),
		"stdOut":       stdOut,
		"stdErr":       stdErr,
		"exitCode":     exitCode,
	}).Info("Finished hook execution")

	response := HookResponse{ExitCode: exitCode, StdOut: stdOut, StdErr: stdErr}
	json, err := json.Marshal(response)
	if err != nil {
		log.WithError(err).Error("Unhandled error in jsonEncode")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(json)
}

func main() {
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)

	port, err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		port = 9999
	}
	address := ":" + strconv.Itoa(port)

	command := "echo 'Hook not configured'"
	if len(os.Getenv("COMMAND")) != 0 {
		command = os.Getenv("COMMAND")
	}
	args = strings.Fields(command)

	log.Info("Starting hook webser on " + address)
	http.HandleFunc("/healthz", healthz)
	http.HandleFunc("/hook", hook)
	http.ListenAndServe(address, nil)
}
