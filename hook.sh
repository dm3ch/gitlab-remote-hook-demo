#!/bin/sh
cd $GIT_DIR
unset GIT_DIR
git show $1

if [[ $(($RANDOM % 2)) == 1 ]]; then
    echo "Random hook fail to proof that blocking hooks works too" >&2
    exit 1
fi
