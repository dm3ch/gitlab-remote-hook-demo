# Proof of concept: Deploy git server hooks as a sidecar of gitaly
Issue: https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2810
## Problem
There's gitlab users who laverages on existing (self-developed) git server hooks.
Unfortenately currently k8s installation method limits your custom hooks to 1MB size (Configmap size limit).
But some of existing hooks could require more space or some custom dependencies. It seems that overcoming this limitation could help some users to migrate to k8s based gitlab installation.

## Existing solutions:
- Currently you can built your own gitaly image (based on official one) including your hooks and all needed dependencies.

  The downside of this solution that you needs to rebuild your image for each gitlab release.
- Migrate workflows to [API based MR checks](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html)

  This option is very interesting and I believe right decision in long-term.
  
  Downsides:
  * requirement of rewriting existing code
  * this feature is going to be Ultimate tier only.

## Proposed solution:
Ship hooks as a separate container running inside gitaly pod (sidecar pattern) sharing volume with repos (same pod so we can work with ReadWriteOnce volumes).

Wrap hook execution to localhost RPC to execute hook in sidecar container and pass data.

Pros:
* Cause of usage of separate container we can start it from completely custom image, so our hooks can be any size and have any dependecies they need
* We can rebuild image only when our hook code changes
* We can keep our existing hooks without rewriting (using external wrappers)

## Demonstration
### Components
Currently my PoC have following components:
* `ruby-hook.rb` - Ruby hook (deployed via configmap) that call makes http remote procedure call on execution and waiting for rpc results
*  `hook-server/` - Golang RPC server, wrapper for actual hook execution. Handles RPC calls and call shell command for each http RPC.
* `hook.sh` - example git hook. Executes `git show <updated_ref>` and randomly returns not zero exit code to check if blocking update from hook works

### How to run
#### Build own hook docker image
```shell
docker build .
docker build . -t <image_repo>
docker push <image_repo>
```

#### Install gitlab helm chart
First eplace `<image_repo>` to actual hook image repo first in `values.yaml`
```shell
helm repo add gitlab https://charts.gitlab.io/

kubectl -n gitlab \
    create configmap my-hook \
    --from-file=ruby-hook.rb

helm upgrade --install \
    -n gitlab gitlab gitlab/gitlab \
    -f values.yaml \
    --set global.hosts.domain=<domain> \
    --set certmanager-issuer.email=<email> \
```

#### Testing
* Create some repo in your newly created gitlab instance (could possibly fail due to randomly added errors in hook, haven't tested that)
* Push new commits to created repo