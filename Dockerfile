FROM golang:1.17-alpine as build
COPY hook-server /app
WORKDIR /app
RUN go build -o /hook-server
EXPOSE 8080

FROM alpine
RUN apk add git
COPY --from=build /hook-server /hook-server
COPY hook.sh /hook.sh
ENV COMMAND "/hook.sh"
ENV PORT "9999"
CMD [ "/hook-server" ]