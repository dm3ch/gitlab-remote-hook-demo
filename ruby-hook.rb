#!/usr/bin/env ruby
require 'faraday'
require 'json'

puts 'Debug: Hook executed'

# Don't know why, but don't work if localhost specified instead of 127.0.0.1
# Haven't debug it yet, but assume it could be caused by resolving localhost to IPv6 problem
url = 'http://127.0.0.1:9999/hook'

body = {}
body['env'] = ENV.to_h.select{ |k,v| k.start_with?("GL_", "GIT_") }
body['args'] = ARGV

response = Faraday.put(url, body.to_json, "Content-Type" => "application/json")
json_response = JSON.parse(response.body)

STDERR.puts json_response['stdErr']
puts json_response['stdOut']
exit json_response['exitCode']